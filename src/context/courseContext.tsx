import { db } from "@/config/firebase_config";
import { collection, doc, getDocs, getDoc} from "firebase/firestore";
import { createContext, useContext, useState, useEffect, ReactNode } from "react";


type firebaseCourse = {
    courses: Course
}
type coursContextType = {
    allCourses: firebaseCourse[],
    selectedCourse: Course | undefined,
    getAllCourses: () => Promise<firebaseCourse[] | null>,
    getCoursbyID: (id: string) => Promise<Course | null>,
    error: string
}

const CoursContext = createContext<coursContextType | undefined>(undefined);

export const useCours = () => {
    const context = useContext(CoursContext);
    if (context == undefined) {
        throw new Error("useCours must be used within an courseProvider");
    }
    return context;
}

interface CoursProviderProps {
    children: ReactNode;
}

export const CoursProvider = ({ children }: CoursProviderProps) => {
    const [allCourses, setAllCourses] = useState<firebaseCourse[]>([])
    const [selectedCourse, setselectedCourse] = useState<Course>()
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState("");

    const getAllCourses = async (): Promise<firebaseCourse[] | null> => {
        const collectionRef = collection(db, 'courses');
    
        try {
            const querySnapshot = await getDocs(collectionRef);
    
            if (querySnapshot.empty) {
                console.log('No courses found.');
                return null;
            }
    
            const courses = querySnapshot.docs.map((doc) => doc.data() as firebaseCourse);
            setAllCourses(courses);
            return courses;
        } catch (error) {
            console.error('Error getting courses:', error);
            setError(`${error}`)
            return null;
        }
    };

    const getCoursbyID = async (id: string): Promise<Course | null> => {
        const courseRef = doc(db, 'courses', id);

        try {
            const courseSnapshot = await getDoc(courseRef);

            if (!courseSnapshot.exists()) {
                console.log('Course not found.');
                return null;
            }

            const course = courseSnapshot.data() as Course;
            setselectedCourse(course)
            return course;
        } catch (error) {
            console.error('Error getting course by ID:', error);
            setError(`${error}`)
            return null;
        }
    }

    const value: coursContextType = {
        allCourses,
        selectedCourse,
        getAllCourses,
        getCoursbyID,
        error
    };

    useEffect(() => {
        getAllCourses().finally(() => setLoading(false));
    }, []);

    return (
        <CoursContext.Provider value={value}>
            {!loading ? children : null}
        </CoursContext.Provider>
    )
}

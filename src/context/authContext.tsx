import {
  createContext,
  useContext,
  useState,
  useEffect,
  ReactNode,
} from "react";
import {
  GoogleAuthProvider,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  signInWithPopup,
  signOut,
} from "firebase/auth";
import { auth, db } from "@/config/firebase_config";
import { User as firebaseUser } from "firebase/auth";
import { collection, doc, onSnapshot, setDoc } from "firebase/firestore";

// Définissez un type pour le context
type AuthContextType = {
  currentUser: User | undefined;
  setCurrentUser: React.Dispatch<React.SetStateAction<User | undefined>>;
  fbUser: firebaseUser | undefined;
  error: string;
  setError: React.Dispatch<React.SetStateAction<string>>;
  login: (login: login) => Promise<User | null>;
  loginWithGoogle: () => Promise<User | null>;
  register: (login: login, userInfo: User) => Promise<User | null>;
  logout: () => void;
};

const AuthContext = createContext<AuthContextType | undefined>(undefined);

export const useAuth = () => {
  const context = useContext(AuthContext);
  if (context == undefined) {
    throw new Error("useAuth must be used within an AuthProvider");
  }
  return context;
};

interface AuthProviderProps {
  children: ReactNode;
}

export function AuthProvider({ children }: AuthProviderProps) {
  const [fbUser, setfbUser] = useState<firebaseUser>();
  const [currentUser, setCurrentUser] = useState<User>();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");

  const register = async (login: login, userInfo: User) => {
    const credential = await createUserWithEmailAndPassword(
      auth,
      login.username,
      login.password
    );
    if (credential) {
      const user: User = {
        ...userInfo,
        uid: credential.user.uid,
      };
      const uid = credential.user.uid;
      const colletionRef = collection(db, "users");
      const docRef = doc(colletionRef, uid);
      await setDoc(docRef, user);
      return new Promise<User | null>((resolve) => {
        onSnapshot(docRef, (doc) => {
          if (doc.exists()) {
            const userData = doc.data();
            setCurrentUser(userData as User);
            resolve(userData as User);
          } else {
            console.log("User does not exist in Firestore.");
            resolve(null);
          }
        });
      });
    } else {
      return null;
    }
  };

  const login = async (login: login): Promise<User | null> => {
    try {
      const credential = await signInWithEmailAndPassword(
        auth,
        login.username,
        login.password
      );
      if (credential) {
        localStorage.setItem("authUser", JSON.stringify(credential.user));
        const uid = credential.user.uid;
        const colletionRef = collection(db, "users");
        const docRef = doc(colletionRef, uid);

        return new Promise<User | null>((resolve) => {
          onSnapshot(docRef, (doc) => {
            if (doc.exists()) {
              const userData = doc.data();
              setCurrentUser(userData as User);
              resolve(userData as User);
            } else {
              console.log("User does not exist in Firestore.");
              resolve(null);
            }
          });
        });
      } else {
        return null;
      }
    } catch (e) {
      setError(error as string);
      throw error;
    }
  };

  const loginWithGoogle = async (): Promise<User | null> => {
    try {
      const provider = new GoogleAuthProvider();
      const result = await signInWithPopup(auth, provider);
      if (result) {
        localStorage.setItem("authUser", JSON.stringify(result.user));
        const uid = result.user.uid;
        const colletionRef = collection(db, "users");
        const docRef = doc(colletionRef, uid);

        return new Promise<User | null>((resolve) => {
          onSnapshot(docRef, async (doc) => {
            if (doc.exists()) {
              const userData = doc.data();
              setCurrentUser(userData as User);
              resolve(userData as User);
            } else {
              const userInfo: User = {
                uid: result.user.uid,
                photoURL: result.user.photoURL ? result.user.photoURL : "",
                image: "",
                name: result.user.displayName ? result.user.displayName : "",
                displayName: result.user.displayName
                  ? result.user.displayName
                  : "",
                username: result.user.displayName
                  ? result.user.displayName
                  : "",
                phoneNumber: result.user.phoneNumber
                  ? result.user.phoneNumber
                  : "",
                email: result.user.email!,
                age: 0,
                points: 0,
                badge: "beginner",
                courses: [],
                pieces: 0,
                participatedEvents: [],
              };
              await setDoc(docRef, userInfo);
              onSnapshot(docRef, (doc) => {
                if (doc.exists()) {
                  const userData = doc.data();
                  setCurrentUser(userData as User);
                  resolve(userData as User);
                } else {
                  console.log("User does not exist in Firestore.");
                  resolve(null);
                }
              });
            }
          });
        });
      } else {
        return null;
      }
    } catch (error) {
      setError(error as string);
      throw error;
    }
  };

  function logout() {
    return signOut(auth);
  }

  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged((user) => {
      setfbUser(user!);
      setLoading(false);

      if (user) {
        const uid = user.uid;
        const colletionRef = collection(db, "users");
        const docRef = doc(colletionRef, uid);

        const unsubscribeFirestore = onSnapshot(docRef, (doc) => {
          if (doc.exists()) {
            const userData = doc.data();
            setCurrentUser(userData as User);
          } else {
            console.log("User does not exist in Firestore.");
            setCurrentUser(undefined);
          }
        });

        return () => unsubscribeFirestore();
      } else {
        setCurrentUser(undefined);
      }
    });

    return () => unsubscribe();
  }, []);

  const value: AuthContextType = {
    currentUser,
    setCurrentUser,
    fbUser,
    error,
    setError,
    login,
    loginWithGoogle,
    register,
    logout,
  };

  return (
    <AuthContext.Provider value={value}>
      {!loading ? children : null}
    </AuthContext.Provider>
  );
}

import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import RootLayout from "./app/RootLayout";
import "./global.css";
import HomePage from "./app/home/HomePage";
import CoursesPage from "./app/courses/CoursesPage";
import ErrorPage from "./app/ErrorPage";
import EventsPage from "./app/events/EventsPage";
import CourseDetail from "./app/courses/id/CourseDetail";
import { ThemeProvider } from "./providers/themeProviders";
import { LoginPage } from "./app/login/loginPage";
import { AuthProvider } from "./context/authContext";
import CourseLayout from "./app/courses/id/CourseLayout";
import CoursePage from "./app/courses/id/CoursePage";
import CoursesLayout from "./app/courses/CoursesLayout";
import EventsLayout from "./app/events/EventsLayout";
import EventDetail from "./app/events/id/EventDetail";
import { QuizPage } from "./app/courses/id/QuizPage";
import { RegisterPage } from "./app/register/register";
import { EventDeployPage } from "./app/events/id/EventDeployPage";
import { CoursProvider } from "./context/courseContext";
import ProfilePage from "./app/profile/ProfilePage";
import BuyPiecePage from "./app/buy-pieces/BuyPiecePage";
import WithdrawalPage from "./app/withdrawal/WithdrawalPage";

const router = createBrowserRouter([
  {
    path: "/",
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "/",
        element: <HomePage />,
      },
      {
        path: "/courses",
        element: <CoursesLayout />,
        children: [
          {
            path: "/courses",
            element: <CoursesPage />,
          },
          {
            path: "/courses/:courseId",
            element: <CoursePage />,
          },
        ],
      },
      {
        path: "/courses/:courseId/modules/:moduleId",
        element: <CourseLayout />,
        children: [
          {
            path: "/courses/:courseId/modules/:moduleId",
            element: <CourseDetail />,
          },
        ],
      },
      {
        path: "/events",
        element: <EventsLayout />,
        children: [
          {
            path: "/events",
            element: <EventsPage />,
          },
          {
            path: "/events/:eventId",
            element: <EventDetail />,
          },
          {
            path: "/events/:eventId/deploy",
            element: <EventDeployPage />,
          },
        ],
      },
      {
        path: "/courses/:courseId/quizzes/:quizId",
        element: <QuizPage />,
      },
      {
        path: "/login",
        element: <LoginPage />,
      },
      {
        path: "/buy-pieces",
        element: <BuyPiecePage />,
      },
      {
        path: "/withdrawal",
        element: <WithdrawalPage />,
      },
      {
        path: "/profile/:userId",
        element: <ProfilePage />,
      },
      {
        path: "/regiter",
        element: <RegisterPage />,
      },
    ],
  },
]);

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <ThemeProvider>
      <CoursProvider>
        <AuthProvider>
          <RouterProvider router={router} />
        </AuthProvider>
      </CoursProvider>
    </ThemeProvider>
  </React.StrictMode>
);

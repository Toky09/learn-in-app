export const events: ChallengeEvent[] = [
  {
    id: "1",
    title: "Product Showcase Card Challenge",
    image:
      "https://img.freepik.com/vecteurs-libre/place-technologie-du-futur-flyer_23-2148756650.jpg?z=900",
    description:
      "Design a visually appealing product showcase card using HTML and CSS. Your task is to create a reusable card component with the following requirements:\n- Display product image, name, and a brief description.\n- Include a button to view more details about the product.\n- Ensure a responsive layout for different screen sizes.\n### Extra Feature\nImplement a hover effect to showcase additional product details.\n### Animation\nAdd smooth transitions for hover effects.",
    requirementBadge: "beginner",
    participatedPrize: 2,
    prizes: {
      first: 100,
      second: 50,
      third: 20,
    },
    winners: [],
    createdAt: new Date("2023-11-09T08:00:00Z"),
    expireAfter: new Date("2023-12-09T08:00:00Z"),
  },
  {
    id: "2",
    title: "User Profile Card Design",
    image:
      "https://img.freepik.com/vecteurs-libre/infographie-personnalite-acheteur-au-design-plat_23-2148659313.jpg?=900",
    description: `
        # User Profile Card Design
        
        Create an aesthetically pleasing user profile card using HTML and CSS. Your task is to design a card component with the following requirements:
        
        - Display user avatar, username, and a short bio.
        - Include icons for social media links (e.g., GitHub, LinkedIn).
        - Ensure the card is visually appealing on both desktop and mobile.
        
        ### Extra Feature
        
        Add a "Follow" button with a dynamic follower count.
        
        ### Animation
        
        Implement a subtle animation for the follow button.
      `,
    requirementBadge: "bronze",
    participatedPrize: 3,
    prizes: {
      first: 150,
      second: 80,
      third: 40,
    },
    createdAt: new Date("2023-11-10T10:00:00Z"),
    expireAfter: new Date("2023-12-10T10:00:00Z"),
    winners: [],
  },
  {
    id: "3",
    title: "Interactive Pricing Table",
    image:
      "https://img.freepik.com/vecteurs-libre/plans-elegants-modele-comparaison-prix_1017-32014.jpg?w=900",
    description: `
        # Interactive Pricing Table
        
        Develop an interactive pricing table using HTML, CSS, and JavaScript. Your task is to create a table component with the following requirements:
        
        - Display different pricing plans with features.
        - Allow users to toggle between monthly and yearly pricing.
        - Highlight the recommended plan.
        
        ### Extra Feature
        
        Implement a pricing slider for custom plan selection.
        
        ### Animation
        
        Use animations for toggling between monthly and yearly pricing.
      `,
    requirementBadge: "silver",
    participatedPrize: 4,
    prizes: {
      first: 200,
      second: 100,
      third: 50,
    },
    createdAt: new Date("2023-11-11T12:00:00Z"),
    expireAfter: new Date("2023-12-11T12:00:00Z"),
    winners: [],
  },
  {
    id: "4",
    title: "Responsive Navigation Bar",
    image:
      "https://img.freepik.com/vecteurs-libre/collection-elements-ui-ux-plats_23-2149053978.jpg?w=900",
    description: `
        # Responsive Navigation Bar
        
        Design a responsive navigation bar using HTML and CSS. Your task is to create a navigation component with the following requirements:
        
        - Include navigation links and a logo.
        - Implement a hamburger menu for smaller screens.
        - Ensure smooth transitions between mobile and desktop views.
        
        ### Extra Feature
        
        Add a search bar to the navigation menu.
        
        ### Animation
        
        Implement a sliding animation for the hamburger menu.
      `,
    requirementBadge: "gold",
    participatedPrize: 5,
    prizes: {
      first: 250,
      second: 120,
      third: 60,
    },
    createdAt: new Date("2023-11-12T14:00:00Z"),
    expireAfter: new Date("2023-12-12T14:00:00Z"),
    winners: [],
  },
  {
    id: "5",
    title: "Animated Progress Bar",
    image:
      "https://img.freepik.com/vecteurs-libre/collection-curseurs-degrade_23-2149201896.jpg?w=900",
    description: `
        # Animated Progress Bar
        
        Create a visually appealing and animated progress bar using HTML, CSS, and JavaScript. Your task is to design a progress bar component with the following requirements:
        
        - Display progress percentage with animation.
        - Allow users to interact with the progress bar.
        - Include a label indicating the current progress.
        
        ### Extra Feature
        
        Customize the color and style of the progress bar.
        
        ### Animation
        
        Implement dynamic animations for progress changes.
      `,
    requirementBadge: "diamond",
    participatedPrize: 6,
    prizes: {
      first: 300,
      second: 150,
      third: 70,
    },
    createdAt: new Date("2023-11-13T16:00:00Z"),
    expireAfter: new Date("2023-12-13T16:00:00Z"),
    winners: [],
  },
  {
    id: "6",
    title: "Chat Application UI",
    image:
      "https://img.freepik.com/vecteurs-libre/concept-interface-application-rencontres_23-2148530533.jpg?w=900",
    description: `
        # Chat Application UI
        
        Design a modern and user-friendly chat application UI using HTML and CSS. Your task is to create a chat interface with the following requirements:
        
        - Display user avatars, usernames, and messages.
        - Include a sidebar for online users.
        - Ensure a responsive layout for various screen sizes.
        
        ### Extra Feature
        
        Implement real-time message updates using mock data.
        
        ### Animation
        
        Use subtle animations for message transitions.
      `,
    requirementBadge: "gold",
    participatedPrize: 4,
    prizes: {
      first: 180,
      second: 90,
      third: 30,
    },
    createdAt: new Date("2023-11-14T18:00:00Z"),
    expireAfter: new Date("2023-12-14T18:00:00Z"),
    winners: [],
  },
  {
    id: "7",
    title: "Weather Widget",
    image:
      "https://img.freepik.com/vecteurs-libre/theme-elegant-ecran-accueil-pour-smartphone_23-2148726370.jpg?w=900",
    description: `
        # Weather Widget
        
        Create a visually appealing weather widget using HTML, CSS, and a weather API. Your task is to design a widget with the following requirements:
        
        - Display current weather conditions, temperature, and location.
        - Include icons or graphics representing the weather.
        - Ensure the widget is visually appealing on both desktop and mobile.
        
        ### Extra Feature
        
        Add a forecast for the next few days.
        
        ### Animation
        
        Implement dynamic animations for weather updates.
      `,
    requirementBadge: "silver",
    participatedPrize: 3,
    prizes: {
      first: 120,
      second: 60,
      third: 20,
    },
    createdAt: new Date("2023-11-15T20:00:00Z"),
    expireAfter: new Date("2023-12-15T20:00:00Z"),
    winners: [],
  },
];

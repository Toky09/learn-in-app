export const courses: Course[] = [
  {
    id: "1",
    image:
      "https://images.pexels.com/photos/270404/pexels-photo-270404.jpeg?auto=compress&cs=tinysrgb&w=700/",
    title: "HTML Fundamentals",
    description:
      "## HTML Fundamentals Course Unlock the Language of the Web\n\nWelcome to HTML Fundamentals, your gateway to mastering the art of web development! In this comprehensive course, we'll guide you through the essentials of HTML, empowering you to create well-structured and engaging web pages.\n\n**What You'll Learn:**\n- Web Page Structure: Gain a solid foundation by understanding how to structure your web pages effectively.\n- Text Elements: Learn the art of crafting compelling content with headings, paragraphs, and lists.\n- Navigation: Dive into the world of hyperlinks, enabling seamless navigation within your web pages.\n\n**Why HTML Matters:**\nHTML is the backbone of the internet, serving as the language that structures content on the web. Whether you're a budding web developer, designer, or simply curious about how websites work, this course is your key to unlocking the secrets of HTML.\n\n**Get Started Today:** Embark on your web development journey with HTML Fundamentals. No prior experience required—just a passion for creating on the web. Let's build the future of the internet together!",
    modules: [
      {
        id: "1",
        title: "Introduction to HTML",
        description:
          "Get acquainted with Hypertext Markup Language (HTML), the backbone of web development. This module covers the essentials of HTML and its vital role in creating web pages.",
        link: "https://www.youtube.com/watch?v=ok-plXXHlWw",
        point: 10,
        seen: false,
        disabled: false,
      },
      {
        id: "2",
        title: "HTML Elements",
        description:
          "Dive deeper into HTML by exploring common elements like headings, paragraphs, and lists. Learn how to structure your content effectively.",
        link: "https://www.youtube.com/watch?v=9NEgFrUBGUs",
        point: 15,
        seen: false,
        disabled: true,
      },
    ],
    quizzes: [
      {
        id: "1",
        question: "What does HTML stand for?",
        responses: [
          "Hypertext Markup Language",
          "High Tech Modern Language",
          "Hyper Transfer Markup Language",
        ],
        trueResponse: "Hypertext Markup Language",
        point: 5,
      },
      {
        id: "2",
        question: "Which HTML tag is used for creating hyperlinks?",
        responses: ["<a>", "<h1>", "<p>"],
        trueResponse: "<a>",
        point: 10,
      },
    ],
    cost: 0,
    point: 100,
    rode: false,
    disabled: false,
    difficulty: "ease",
    createdAt: new Date("2023-11-08T10:00:00Z"),
    updatedAt: new Date("2023-11-08T15:30:00Z"),
  },
  {
    id: "2",
    image:
      "https://images.pexels.com/photos/11035386/pexels-photo-11035386.jpeg?auto=compress&cs=tinysrgb&w=700/",
    title: "CSS Essentials",
    description:
      "## CSS Essentials Course Master the Art of Styling\n\nWelcome to CSS Essentials, where you'll delve into the core principles of Cascading Style Sheets. This course empowers you to craft visually appealing and responsive web designs by exploring the intricacies of CSS.\n\n**What You'll Learn:**\n- Selectors and Properties: Understand the power of selectors and how to apply various styling properties.\n- Layout and Flexbox: Dive into the world of layout design and master the flexibility of Flexbox.\n- Responsive Design: Learn techniques to create responsive and adaptive layouts for different devices.\n\n**Why CSS Matters:**\nCSS is the design language of the web, allowing you to transform raw HTML into visually stunning websites. Whether you're a front-end developer, designer, or aspiring UX/UI enthusiast, mastering CSS is essential.\n\n**Get Started Today:** Embark on your design journey with CSS Essentials. No prior experience required—just a creative spirit ready to style the web. Let's bring your designs to life!",
    modules: [
      {
        id: "1",
        title: "Introduction to CSS",
        description:
          "Embark on a journey into the world of CSS, the magic behind web design. This module introduces the power of Cascading Style Sheets and their role in creating visually appealing websites.",
        link: "https://www.youtube.com/watch?v=OEV8gMkCHXQ",
        point: 10,
        seen: false,
        disabled: true,
      },
      {
        id: "2",
        title: "Selectors and Properties",
        description:
          "Explore the art of selecting HTML elements and applying properties to transform the appearance of your web content. This module delves into the essence of CSS styling.",
        link: "https://www.youtube.com/watch?v=1PnVor36_40",
        point: 15,
        seen: false,
        disabled: true,
      },
    ],
    quizzes: [
      {
        id: "1",
        question: "What does CSS stand for?",
        responses: [
          "Cascading Style Sheet",
          "Computer Style Sheet",
          "Creative Style Sheet",
        ],
        trueResponse: "Cascading Style Sheet",
        point: 5,
      },
      {
        id: "2",
        question:
          "Which CSS property is used to set the background color of an element?",
        responses: ["background-color", "color", "font-size"],
        trueResponse: "background-color",
        point: 10,
      },
    ],
    cost: 0,
    point: 120,
    rode: false,
    disabled: false,
    difficulty: "ease",
    createdAt: new Date("2023-11-08T11:00:00Z"),
    updatedAt: new Date("2023-11-08T16:30:00Z"),
  },
  {
    id: "3",
    title: "JavaScript Basics",
    image:
      "https://images.pexels.com/photos/270557/pexels-photo-270557.jpeg?auto=compress&cs=tinysrgb&w=700/",
    description:
      "Unleash the power of interactivity with JavaScript Basics. This course is your gateway to the world of JavaScript programming. Learn the fundamentals of variables, data types, and how to bring life to your web pages.",
    modules: [
      {
        id: "1",
        title: "Introduction to JavaScript",
        description:
          "Discover the magic of JavaScript, a versatile scripting language used to add interactivity to web pages. Dive into its essential concepts and uses.",
        link: "https://example.com/js-intro",
        point: 10,
        seen: false,
        disabled: true,
      },
      {
        id: "2",
        title: "Variables and Data Types",
        description:
          "Master the art of declaring variables and understanding data types in JavaScript. Lay a strong foundation for your coding journey.",
        link: "https://example.com/js-variables",
        point: 15,
        seen: false,
        disabled: true,
      },
    ],
    quizzes: [
      {
        id: "1",
        question: "What is JavaScript primarily used for?",
        responses: [
          "Adding interactivity to web pages",
          "Creating graphics",
          "Sending emails",
        ],
        trueResponse: "Adding interactivity to web pages",
        point: 5,
      },
      {
        id: "2",
        question: "How do you declare a variable in JavaScript?",
        responses: ["var", "let", "const"],
        trueResponse: "let",
        point: 10,
      },
    ],
    cost: 60,
    point: 140,
    rode: false,
    disabled: true,
    difficulty: "medium",
    createdAt: new Date("2023-11-08T12:00:00Z"),
    updatedAt: new Date("2023-11-08T17:30:00Z"),
  },
  {
    id: "4",
    image:
      "https://images.pexels.com/photos/4126743/pexels-photo-4126743.jpeg?auto=compress&cs=tinysrgb&w=700/",
    title: "Responsive Web Design",
    description:
      "Learn the art of creating web designs that adapt to various screen sizes and devices. Explore CSS media queries and flexible layouts to build responsive websites.",
    modules: [
      {
        id: "1",
        title: "Introduction to Responsive Design",
        description:
          "Discover the importance of responsive web design and its impact on user experience. Dive into responsive principles.",
        link: "https://example.com/rwd-intro",
        point: 10,
        seen: false,
        disabled: true,
      },
      {
        id: "2",
        title: "CSS Media Queries",
        description:
          "Learn the power of CSS media queries to tailor your styles to different devices. Create a fluid and responsive layout that looks great on smartphones, tablets, and desktops.",
        link: "https://example.com/rwd-media-queries",
        point: 15,
        seen: false,
        disabled: true,
      },
    ],
    quizzes: [
      {
        id: "1",
        question: "What is the main goal of responsive web design?",
        responses: [
          "Adapting to different screen sizes",
          "Creating fixed-width layouts",
          "Supporting only desktop browsers",
        ],
        trueResponse: "Adapting to different screen sizes",
        point: 5,
      },
      {
        id: "2",
        question: "Which CSS feature is crucial for responsive web design?",
        responses: ["Media queries", "Font styles", "JavaScript functions"],
        trueResponse: "Media queries",
        point: 10,
      },
    ],
    cost: 135,
    point: 160,
    rode: false,
    disabled: true,
    difficulty: "medium",
    createdAt: new Date("2023-11-08T13:00:00Z"),
    updatedAt: new Date("2023-11-08T18:30:00Z"),
  },
  {
    id: "5",
    image:
      "https://images.pexels.com/photos/1181359/pexels-photo-1181359.jpeg?auto=compress&cs=tinysrgb&w=700/",
    title: "Introduction to Python",
    description:
      "Embark on a Python programming journey. Learn the basics of Python, including syntax, data types, and control structures. Python is a versatile and beginner-friendly language.",
    modules: [
      {
        id: "1",
        title: "Getting Started with Python",
        description:
          "Start your Python adventure with an introduction to the language, its syntax, and its use cases.",
        link: "https://example.com/python-start",
        point: 10,
        seen: false,
        disabled: true,
      },
      {
        id: "2",
        title: "Python Data Types",
        description:
          "Explore Python's data types, including numbers, strings, and lists. Get comfortable with data manipulation.",
        link: "https://example.com/python-data-types",
        point: 15,
        seen: false,
        disabled: true,
      },
    ],
    quizzes: [
      {
        id: "1",
        question: "What is Python known for?",
        responses: [
          "Readability and simplicity",
          "High performance",
          "Low-level programming",
        ],
        trueResponse: "Readability and simplicity",
        point: 5,
      },
      {
        id: "2",
        question: "Which data type is used for text in Python?",
        responses: ["str", "int", "list"],
        trueResponse: "str",
        point: 10,
      },
    ],
    cost: 300,
    point: 120,
    rode: false,
    disabled: true,
    difficulty: "medium",
    createdAt: new Date("2023-11-08T14:00:00Z"),
    updatedAt: new Date("2023-11-08T19:30:00Z"),
  },
  {
    id: "6",
    image:
      "https://images.pexels.com/photos/11035471/pexels-photo-11035471.jpeg?auto=compress&cs=tinysrgb&w=700/",
    title: "Web Development with React",
    description:
      "Become a pro in building dynamic web applications with React. This course covers the fundamentals of React, components, and state management.dev)",
    modules: [
      {
        id: "1",
        title: "Introduction to React",
        description:
          "Get started with React, a popular JavaScript library for building user interfaces. Learn its core concepts and benefits.",
        link: "https://example.com/react-intro",
        point: 10,
        seen: false,
        disabled: true,
      },
      {
        id: "2",
        title: "React Components",
        description:
          "Explore React components, the building blocks of React applications. Create reusable UI elements.",
        link: "https://example.com/react-components",
        point: 15,
        seen: false,
        disabled: true,
      },
    ],
    quizzes: [
      {
        id: "1",
        question: "What is React primarily used for?",
        responses: [
          "Building user interfaces",
          "Server-side scripting",
          "Database management",
        ],
        trueResponse: "Building user interfaces",
        point: 5,
      },
      {
        id: "2",
        question: "What is a React component?",
        responses: [
          "A reusable UI element",
          "A database table",
          "A server function",
        ],
        trueResponse: "A reusable UI element",
        point: 10,
      },
    ],
    cost: 375,
    point: 160,
    rode: false,
    disabled: true,
    difficulty: "medium",
    createdAt: new Date("2023-11-08T15:00:00Z"),
    updatedAt: new Date("2023-11-08T20:30:00Z"),
  },
  {
    id: "7",
    image:
      "https://images.pexels.com/photos/8386440/pexels-photo-8386440.jpeg?auto=compress&cs=tinysrgb&w=700/",
    title: "Machine Learning Fundamentals",
    description:
      "Dive into the exciting world of machine learning. Learn the basics of machine learning, data preprocessing, and model building. Start your journey into AI and data science.",
    modules: [
      {
        id: "1",
        title: "Introduction to Machine Learning",
        description:
          "Discover the concepts of machine learning, its applications, and its importance in the modern world.",
        link: "https://example.com/ml-intro",
        point: 10,
        seen: false,
        disabled: true,
      },
      {
        id: "2",
        title: "Data Preprocessing",
        description:
          "Learn how to clean, preprocess, and transform data to make it ready for machine learning tasks.",
        link: "https://example.com/ml-data-preprocessing",
        point: 15,
        seen: false,
        disabled: true,
      },
    ],
    quizzes: [
      {
        id: "1",
        question: "What is machine learning focused on?",
        responses: [
          "Pattern recognition and prediction",
          "Human emotions",
          "Hardware optimization",
        ],
        trueResponse: "Pattern recognition and prediction",
        point: 5,
      },
      {
        id: "2",
        question: "What is data preprocessing in machine learning?",
        responses: [
          "Data cleaning and transformation",
          "Data collection",
          "Model building",
        ],
        trueResponse: "Data cleaning and transformation",
        point: 10,
      },
    ],
    cost: 490,
    point: 200,
    rode: false,
    disabled: true,
    difficulty: "hard",
    createdAt: new Date("2023-11-08T16:00:00Z"),
    updatedAt: new Date("2023-11-08T21:30:00Z"),
  },
  {
    id: "8",
    image:
      "https://images.pexels.com/photos/907489/pexels-photo-907489.jpeg?auto=compress&cs=tinysrgb&w=700/",
    title: "Android App Development",
    description:
      "Learn to develop Android apps with Java and Kotlin. Dive into app design, user interfaces, and app functionality. Start building your mobile app portfolio.",
    modules: [
      {
        id: "1",
        title: "Introduction to Android Development",
        description:
          "Begin your journey in Android app development by learning the basics of creating apps for mobile devices.",
        link: "https://example.com/android-intro",
        point: 10,
        seen: false,
        disabled: true,
      },
      {
        id: "2",
        title: "User Interfaces and Layouts",
        description:
          "Master the art of designing user interfaces and layouts for your Android apps. Create visually appealing mobile experiences.",
        link: "https://example.com/android-ui-layouts",
        point: 15,
        seen: false,
        disabled: true,
      },
    ],
    quizzes: [
      {
        id: "1",
        question:
          "What programming languages are commonly used for Android app development?",
        responses: ["Java and Kotlin", "Python and C++", "JavaScript and HTML"],
        trueResponse: "Java and Kotlin",
        point: 5,
      },
      {
        id: "2",
        question: "What is an essential aspect of Android app development?",
        responses: [
          "Designing user interfaces",
          "Creating complex algorithms",
          "Server configuration",
        ],
        trueResponse: "Designing user interfaces",
        point: 10,
      },
    ],
    cost: 480,
    point: 180,
    rode: false,
    disabled: true,
    difficulty: "hard",
    createdAt: new Date("2023-11-08T17:00:00Z"),
    updatedAt: new Date("2023-11-08T22:30:00Z"),
  },
  {
    id: "9",
    image:
      "https://images.pexels.com/photos/669610/pexels-photo-669610.jpeg?auto=compress&cs=tinysrgb&w=700/",
    title: "Data Science with Python",
    description:
      "Dive into the world of data science using Python. Learn data analysis, visualization, and machine learning with real-world datasets.",
    modules: [
      {
        id: "1",
        title: "Introduction to Data Science",
        description:
          "Begin your journey in data science by understanding the core concepts and applications of data analysis.",
        link: "https://example.com/data-science-intro",
        point: 10,
        seen: false,
        disabled: true,
      },
      {
        id: "2",
        title: "Data Visualization with Python",
        description:
          "Master the art of data visualization using Python libraries like Matplotlib and Seaborn. Communicate insights effectively.",
        link: "https://example.com/data-visualization",
        point: 15,
        seen: false,
        disabled: true,
      },
    ],
    quizzes: [
      {
        id: "1",
        question: "What is data science primarily focused on?",
        responses: [
          "Extracting insights from data",
          "Creating software applications",
          "Network security",
        ],
        trueResponse: "Extracting insights from data",
        point: 5,
      },
      {
        id: "2",
        question:
          "Which Python libraries are commonly used for data visualization?",
        responses: [
          "Matplotlib and Seaborn",
          "NumPy and Pandas",
          "Django and Flask",
        ],
        trueResponse: "Matplotlib and Seaborn",
        point: 10,
      },
    ],
    cost: 700,
    point: 160,
    rode: false,
    disabled: true,
    difficulty: "hard",
    createdAt: new Date("2023-11-08T18:00:00Z"),
    updatedAt: new Date("2023-11-08T23:30:00Z"),
  },
  {
    id: "10",
    image:
      "https://images.pexels.com/photos/11035380/pexels-photo-11035380.jpeg?auto=compress&cs=tinysrgb&w=700/",
    title: "Node.js for Backend Development",
    description:
      "Become a backend developer with Node.js. Learn server-side programming, API development, and database integration. Build powerful web applications.",
    modules: [
      {
        id: "1",
        title: "Introduction to Node.js",
        description:
          "Begin your journey into backend development by mastering Node.js, a runtime for server-side JavaScript.",
        link: "https://example.com/nodejs-intro",
        point: 10,
        seen: false,
        disabled: true,
      },
      {
        id: "2",
        title: "API Development with Express",
        description:
          "Learn how to create robust APIs using Express.js, a popular Node.js framework. Build the foundation for your backend projects.",
        link: "https://example.com/nodejs-api-dev",
        point: 15,
        seen: false,
        disabled: true,
      },
    ],
    quizzes: [
      {
        id: "1",
        question: "What is Node.js used for?",
        responses: [
          "Server-side programming",
          "Frontend web development",
          "Mobile app development",
        ],
        trueResponse: "Server-side programming",
        point: 5,
      },
      {
        id: "2",
        question:
          "Which framework is commonly used for building APIs with Node.js?",
        responses: ["Express.js", "Angular.js", "React.js"],
        trueResponse: "Express.js",
        point: 10,
      },
    ],
    cost: 400,
    point: 180,
    rode: false,
    disabled: true,
    difficulty: "medium",
    createdAt: new Date("2023-11-08T19:00:00Z"),
    updatedAt: new Date("2023-11-09T00:30:00Z"),
  },
  {
    id: "11",
    title: "Ethical Hacking and Cybersecurity",
    image:
      "https://images.pexels.com/photos/5952651/pexels-photo-5952651.jpeg?auto=compress&cs=tinysrgb&w=700/",
    description:
      "Explore the exciting world of ethical hacking and cybersecurity. Learn to protect networks, detect vulnerabilities, and become a cybersecurity expert.",
    modules: [
      {
        id: "1",
        title: "Introduction to Cybersecurity",
        description:
          "Begin your journey into the world of cybersecurity by understanding the importance of protecting digital assets. Learn about the fundamental concepts and strategies to secure information.",
        link: "https://example.com/cybersecurity-intro",
        point: 10,
        seen: false,
        disabled: true,
      },
      {
        id: "2",
        title: "Vulnerability Assessment",
        description:
          "Master the art of identifying vulnerabilities in networks and applications. Explore various tools and techniques to assess and secure systems effectively.",
        link: "https://example.com/vulnerability-assessment",
        point: 15,
        seen: false,
        disabled: true,
      },
    ],
    quizzes: [
      {
        id: "1",
        question: "What is the primary goal of ethical hacking?",
        responses: [
          "Identifying and fixing security issues",
          "Exploiting vulnerabilities",
          "Harming computer systems",
        ],
        trueResponse: "Identifying and fixing security issues",
        point: 5,
      },
      {
        id: "2",
        question: "What is vulnerability assessment in cybersecurity?",
        responses: [
          "Identifying weaknesses and threats",
          "Encrypting data",
          "Configuring firewalls",
        ],
        trueResponse: "Identifying weaknesses and threats",
        point: 10,
      },
    ],
    cost: 1200,
    point: 200,
    rode: false,
    disabled: true,
    difficulty: "hard",
    createdAt: new Date("2023-11-09T00:00:00Z"),
    updatedAt: new Date("2023-11-09T05:30:00Z"),
  },
];

type Badge = "beginner" | "bronze" | "silver" | "gold" | "diamond";

type login = {
  username: string;
  password: string;
};

// type User = {
//   id: string;
//   name: string;
//   username: string;
//   image: string?;
//   age: number;
//   telNumber: number;
//   email: string;
//   progression: number;
//   piece: number;
//   badge: Badge;
//   courses: Course[];
// };

type User = {
  uid?: string;
  photoURL: string?;
  image: string?;
  name: string?;
  displayName: string;
  username: string;
  phoneNumber: string?;
  email: string;
  age: number;
  points: number;
  pieces: number;
  badge: Badge;
  courses: Course[];
  participatedEvents: ChallengeEvent[];
};

type UserWithPassword = User & {
  password: string;
};

type Module = {
  id: string;
  title: string;
  description: string; // it should be a markdown syntax
  link: string; //link video
  point: number;
  seen: boolean;
  disabled;
};

type Quiz = {
  id: string;
  question: string;
  responses: string[];
  trueResponse: string;
  point: number;
};

type Course = {
  id: string;
  title: string;
  description: string; // it should be a markdown syntax
  image: string;
  modules: Module[];
  quizzes: Quiz[];
  cost: number;
  point: number;
  rode: boolean; // if the course is already rode by the user it comes true, else false
  difficulty: "ease" | "medium" | "hard";
  disabled: boolean;
  createdAt: Date;
  updatedAt: Date;
};

type Prizes = {
  first: number;
  second: number;
  third: number;
};

type ChallengeEvent = {
  id: string;
  title: string;
  description: string;
  image: string;
  requirementBadge: Badge;
  participatedPrize: number; // pieces who user take by participated the event
  prizes: Prizes;
  winners: User[]; // 3 users winner
  createdAt: Date;
  expireAfter: Date;
};

import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar";
import { LogOut, User, LucideCircleDollarSign, Coins } from "lucide-react";
import {
  DropdownMenu,
  DropdownMenuTrigger,
  DropdownMenuContent,
  DropdownMenuItem,
} from "./ui/dropdown-menu";
import { useNavigate } from "react-router-dom";

type Props = {
  currentUser: User;
  logout: () => void;
};

export function UserAvatar({ currentUser, logout }: Props) {
  const navigate = useNavigate();

  return (
    <DropdownMenu>
      <DropdownMenuTrigger asChild>
        <div className="flex items-center gap-2 p-1 pl-4 border rounded-full cursor-pointer">
          <span>
            {currentUser.displayName ?? (currentUser.username || "User")}
          </span>
          <Avatar>
            <AvatarImage
              src={currentUser.photoURL! ?? currentUser.image}
              alt={currentUser.displayName ?? currentUser.username}
            />
            <AvatarFallback className="uppercase">
              {currentUser.displayName
                ? currentUser.displayName.slice(0, 2)
                : currentUser.username
                ? currentUser.username.slice(0, 2)
                : "U"}
            </AvatarFallback>
          </Avatar>
        </div>
      </DropdownMenuTrigger>
      <DropdownMenuContent align="end">
        <DropdownMenuItem
          onClick={() => {
            navigate(`/buy-pieces`);
          }}
          className="flex items-center gap-2 p-1"
        >
          <Coins size={16} />
          <span>Buy Pieces</span>
        </DropdownMenuItem>
        <DropdownMenuItem
          onClick={() => {
            navigate(`/withdrawal`);
          }}
          className="flex items-center gap-2 p-1"
        >
          <LucideCircleDollarSign size={16} />
          <span>Withdrawal</span>
        </DropdownMenuItem>
        <DropdownMenuItem
          onClick={() => {
            navigate(`profile/${currentUser.uid}`);
          }}
          className="flex items-center gap-2 p-1"
        >
          <User size={16} />
          <span>Profile</span>
        </DropdownMenuItem>
        <DropdownMenuItem
          onClick={() => logout()}
          className="flex items-center gap-2 p-1"
        >
          <LogOut size={16} />
          <span>Log Out</span>
        </DropdownMenuItem>
      </DropdownMenuContent>
    </DropdownMenu>
  );
}

import { useNavigate } from "react-router-dom";
import { Button, ButtonProps } from "./ui/button";
import { ChevronLeft } from "lucide-react";
import { twMerge } from "tailwind-merge";

const BackButton = ({ className, ...props }: ButtonProps) => {
  const navigate = useNavigate();
  return (
    <Button
      {...props}
      variant="outline"
      size="sm"
      className={twMerge("w-12 h-12", className)}
      onClick={() => navigate(-1)}
    >
      <ChevronLeft />
    </Button>
  );
};

export default BackButton;

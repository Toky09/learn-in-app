import { cn } from "@/lib/utils";
import { LockClosedIcon } from "@radix-ui/react-icons";
import { useNavigate } from "react-router-dom";
import { twMerge } from "tailwind-merge";

type Props = {
  id: string;
  title: string;
  cost: number;
  imageSrc: string;
  difficulty: "ease" | "medium" | "hard";
  disabled: boolean;
};

const CardCourse = ({
  id,
  imageSrc,
  title,
  cost,
  difficulty,
  disabled = false,
}: Props) => {
  const navigate = useNavigate();

  return (
    <button
      disabled={disabled}
      onClick={() => navigate(`/courses/${id}`)}
      className={twMerge(
        "block relative rounded-tl-2xl rounded-br-2xl w-80 sm:w-full h-60 overflow-hidden border-2 border-orange-500 p-4 pb-2",
        disabled && "border-foreground/60"
      )}
    >
      {disabled && (
        <div className="absolute top-0 left-0 w-full h-full z-20 backdrop-brightness-75 bg-background opacity-80 flex justify-center items-center">
          <LockClosedIcon className="text-foreground w-24 h-24 opacity-75" />
        </div>
      )}
      <div className="relative h-2/3 w-full overflow-hidden rounded-sm brightness-75 bg-foreground/75">
        <img
          src={imageSrc}
          alt={title}
          className="w-full h-full absolute top-0 left-0 object-cover"
        />
      </div>
      <h2 className="text-lg font-semibold pt-2 text-slate-700 dark:text-slate-100 w-full text-start text-ellipsis line-clamp-1">
        {title}
      </h2>

      <div className="flex justify-between items-center pt-1">
        <p className="text-sm text-neutral-400 ">
          Cost: <span>{cost}</span>p
        </p>

        <span
          className={cn(
            "capitalize text-sm font-semibold border rounded-full px-2",
            {
              "text-blue-500": difficulty === "ease",
              "text-green-500": difficulty === "medium",
              "text-yellow-500": difficulty === "hard",
            }
          )}
        >
          {difficulty}
        </span>
      </div>
    </button>
  );
};

export default CardCourse;

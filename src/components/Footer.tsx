const Footer = () => {
  return (
    <footer className="text-opacity-75 text text-center pb-2 pt-3 border-t">
      copyright©2023 - See source code{" "}
      <span className="text-blue-500">here</span>.
    </footer>
  );
};

export default Footer;

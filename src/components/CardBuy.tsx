import { formatMoney } from "@/helpers/moneyFormater";
import { Button } from "./ui/button";

type Props = {
  nbPieces: number;
  cost: number;
};

const CardBuy = ({ nbPieces, cost }: Props) => {
  return (
    <div className="flex flex-col items-center justify-between w-48 h-48 px-4 pt-4 pb-3 border-2 rounded-lg">
      <span className="text-2xl italic text-stone-500">
        {formatMoney(cost, 0)}
        <span className="text-lg italic text-stone-500">&nbsp;Ar</span>
      </span>
      <span>{" to "}</span>
      <span className="text-2xl italic text-orange-500">
        {nbPieces}
        <span className="text-lg italic text-orange-700">&nbsp;pieces</span>
      </span>
      <Button size="lg" className="w-full max-w-none">
        Buy
      </Button>
    </div>
  );
};

export default CardBuy;

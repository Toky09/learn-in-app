const Logo = () => {
  return (
    <a href="/" className="text-3xl font-bold">
      <span className="text-blue-500">L</span>
      <span className="text-orange-500">E</span>
      <span>arn-In</span>
    </a>
  );
};

export default Logo;

import { cn } from "@/lib/utils";
import { ChevronRight } from "lucide-react";
import { buttonVariants } from "./ui/button";
import { useNavigate } from "react-router-dom";

type Props = {
  id: string;
  title: string;
  disabled: boolean;
  courseId: string;
  activate: boolean;
};

const ModuleButton = ({ id, title, disabled, courseId, activate }: Props) => {
  const navigate = useNavigate();

  return (
    <button
      disabled={disabled}
      onClick={() => navigate(`/courses/${courseId}/modules/${id}`)}
      className={cn(
        buttonVariants({ size: "lg", variant: "outline" }),
        "flex items-center justify-between w-full py-8",
        { "bg-muted/75": activate }
      )}
    >
      <span>{title}</span>
      <ChevronRight size={16} />
    </button>
  );
};

export default ModuleButton;

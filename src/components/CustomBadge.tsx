import { cn } from "@/lib/utils";
import { Badge } from "./ui/badge";

type Props = {
  badge: Badge;
};

const CustomBadge = ({ badge }: Props) => {
  return (
    <Badge
      variant={"outline"}
      className={cn("rounded-full capitalize", {
        "dark:border-violet-400 border-violet-800 dark:text-violet-400 text-violet-800":
          badge === "beginner",
        "border-bronze text-bronze": badge === "bronze",
        "border-silver text-silver": badge === "silver",
        "border-gold text-gold": badge === "gold",
        "border-diamond text-diamond": badge === "diamond",
      })}
    >
      {badge}
    </Badge>
  );
};

export default CustomBadge;

import { useNavigate } from "react-router-dom";
import Logo from "./Logo";
import { Button } from "./ui/button";
import { ToggleThemeButton } from "./ui/toggleThemeButton";
import { useAuth } from "@/context/authContext";
import { UserAvatar } from "./UserAvatar";

const Header = () => {
  const navigate = useNavigate();
  const { currentUser, logout } = useAuth();

  return (
    <header className="flex justify-between items-center p-4 border-b border-muted-foreground/60 shadow-md dark:shadow-none">
      <Logo />
      <div className="flex items-center gap-2">
        <ToggleThemeButton />
        <Button
          variant="outline"
          onClick={() => navigate("/courses")}
          className="font-bold"
        >
          Courses
        </Button>
        <div className="flex items-center gap-2">
          {!currentUser ? (
            <Button
              className="bg-orange-500 font-bold"
              onClick={() => navigate("/login")}
            >
              Sign In
            </Button>
          ) : (
            <>
              <Button
                variant="outline"
                onClick={() => navigate("/events")}
                className="font-bold"
              >
                Events
              </Button>
              <UserAvatar currentUser={currentUser} logout={logout} />
            </>
          )}
        </div>
      </div>
    </header>
  );
};

export default Header;

import { useNavigate } from "react-router-dom";
import CustomBadge from "./CustomBadge";
import { format } from "date-fns";

type Props = {
  challengeEvent: ChallengeEvent;
};

const EventCard = ({ challengeEvent }: Props) => {
  const navigate = useNavigate();

  return (
    <div
      onClick={() => navigate(`/events/${challengeEvent.id}`)}
      className="cursor-pointer relative rounded-tl-2xl rounded-br-2xl max-w-md w-full h-[26rem] overflow-hidden border-2 border-orange-500 p-4 pb-2"
    >
      <div className="relative h-2/3 w-full overflow-hidden rounded-sm brightness-75 bg-foreground/75">
        <img
          src={challengeEvent.image}
          alt={challengeEvent.title}
          className="w-full h-full absolute top-0 left-0 object-cover"
        />
      </div>
      <h2 className="text-lg font-semibold pt-2 text-slate-700 dark:text-slate-100 w-full text-start text-ellipsis line-clamp-1 border-b pb-1">
        {challengeEvent.title}
      </h2>

      <div className="flex flex-col">
        <span>
          <b>First Prize : </b>
          <span className="text-orange-500 text-lg italic">
            {challengeEvent.prizes.first}
            <span className="italic text-sm text-orange-700">&nbsp;pieces</span>
          </span>
        </span>
        <span>
          <b>Requirement : </b>
          <CustomBadge badge={challengeEvent.requirementBadge} />
        </span>
        <span>
          <b>Expired Date : </b>
          <span className="text-muted-foreground text-sm italic">
            {format(challengeEvent.expireAfter, "eeee dd MMM yyyy, HH:mm OOOO")}
          </span>
        </span>
      </div>
    </div>
  );
};

export default EventCard;

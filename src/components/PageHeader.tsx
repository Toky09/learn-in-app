import CustomBadge from "./CustomBadge";

type Props = {
  pageTitle: string;
  piece: number;
};

const PageHeader = ({ pageTitle, piece }: Props) => {
  return (
    <div className="flex justify-between items-center px-6 pt-4 pb-3 mb-6">
      <h1 className="text-2xl font-semibold">{pageTitle}</h1>
      <div className="flex  gap-2">
        <span>
          {" "}
          <b>My Pieces : </b>
          <span className="font-semibold text-md">{piece}</span>
          <span className="italic text-sm">&nbsp;piece{piece > 1 ?? "s"}</span>
        </span>
        <span>
          {"| "}
          <b>Badge : </b>
          <CustomBadge badge="beginner" />
        </span>
      </div>
    </div>
  );
};

export default PageHeader;

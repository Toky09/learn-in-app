// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import {getFirestore} from "firebase/firestore"
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyD7RB5FMkW70BN_qbFdBiBDn6GZteDVQmw",
  authDomain: "lernin-55c7d.firebaseapp.com",
  projectId: "lernin-55c7d",
  storageBucket: "lernin-55c7d.appspot.com",
  messagingSenderId: "1098667186888",
  appId: "1:1098667186888:web:6462d57cd09750cfc0e3fd"
};

const app = initializeApp(firebaseConfig); 
const auth = getAuth();
const db = getFirestore(app);

export {auth, db}

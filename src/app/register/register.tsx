import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import * as z from "zod";

import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { toast } from "@/components/ui/use-toast";
import { useNavigate } from "react-router-dom";
import { useAuth } from "@/context/authContext";

const FormSchema = z
  .object({
    name: z.string().min(2, {
      message: "Name must be at least 2 characters.",
    }),
    username: z.string().min(2, {
      message: "Username must be at least 2 characters.",
    }),
    email: z.string().email({
      message: "Invalid email address.",
    }),
    password: z.string().min(8, {
      message: "Password must be at least 8 characters.",
    }),
    confirmPassword: z.string().min(8, {
      message: "Password must be at least 8 characters.",
    }),
  })
  .superRefine(({ confirmPassword, password }, ctx) => {
    if (confirmPassword !== password) {
      ctx.addIssue({
        code: "custom",
        message: "The passwords did not match",
      });
    }
  });

export const RegisterPage = () => {
  const navigate = useNavigate();
  const { loginWithGoogle, register } = useAuth();

  const form = useForm<z.infer<typeof FormSchema>>({
    resolver: zodResolver(FormSchema),
    defaultValues: {
      name: "",
      username: "",
      email: "",
      password: "",
      confirmPassword: "",
    },
  });

  const onSubmit = async (data: z.infer<typeof FormSchema>) => {
    const logindata: login = { username: data.email, password: data.password };
    const userInfo: User = {
      photoURL: "",
      image: "",
      name: data.name,
      displayName: "",
      username: data.username,
      phoneNumber: "",
      email: data.email,
      age: 0,
      points: 0,
      badge: "beginner",
      courses: [],
      pieces: 0,
      participatedEvents: [],
    };
    try {
      const credential = await register(logindata, userInfo);
      if (credential) {
        navigate("/login");
        toast({
          title: `Welcome back ` + credential.name,
        });
      }
    } catch (e) {
      console.log("Erreur lors de la connexion avec Google", e);
    }
  };

  const logWithgoogle = async () => {
    const user = await loginWithGoogle();
    if (user) {
      navigate("/");
      toast({
        title: `Welcome back ` + user?.name,
      });
    }
  };

  return (
    <div className="flex justify-center items-center pt-10 pb-10">
      <div>
        <div className="border rounded-lg border-opacity-0 shadow-md p-4 w-80">
          <div className="text-center mb-4">
            <h1 className="font-bold text-2xl">Join us!</h1>
            <h1 className="text-sm">Please enter your details</h1>
          </div>
          <Form {...form}>
            <form
              onSubmit={form.handleSubmit(onSubmit)}
              className="w-full space-y-6"
            >
              {/* ... Other form fields remain unchanged ... */}
              <FormField
                control={form.control}
                name="name"
                render={({ field }) => (
                  <>
                    <FormItem>
                      <FormLabel className="font-bold">Name</FormLabel>
                      <FormControl>
                        <Input placeholder="Name" {...field} />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  </>
                )}
              />

              <FormField
                control={form.control}
                name="username"
                render={({ field }) => (
                  <>
                    <FormItem>
                      <FormLabel className="font-bold">UserName</FormLabel>
                      <FormControl>
                        <Input placeholder="userName" {...field} />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  </>
                )}
              />

              <FormField
                control={form.control}
                name="email"
                render={({ field }) => (
                  <>
                    <FormItem>
                      <FormLabel className="font-bold">Email</FormLabel>
                      <FormControl>
                        <Input type="email" placeholder="Email" {...field} />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  </>
                )}
              />

              <FormField
                control={form.control}
                name="password"
                render={({ field }) => (
                  <>
                    <FormItem>
                      <FormLabel className="font-bold">Password</FormLabel>
                      <FormControl>
                        <Input
                          type="password"
                          placeholder="Password"
                          {...field}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  </>
                )}
              />

              <FormField
                control={form.control}
                name="confirmPassword"
                render={({ field }) => (
                  <>
                    <FormItem>
                      <FormLabel className="font-bold">
                        Confirm Password
                      </FormLabel>
                      <FormControl>
                        <Input
                          type="password"
                          placeholder="Confirm Password"
                          {...field}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  </>
                )}
              />

              <Button type="submit" className="w-full font-bold">
                Sign Up
              </Button>
            </form>
          </Form>
          <p className="text-center my-2">or</p>
          <Button
            type="button"
            className="w-full font-bold"
            onClick={logWithgoogle}
          >
            Sign Up with Google
          </Button>
        </div>
        <p className="text-center mt-2 text-sm">
          Already have an account ?{" "}
          <a
            className="font-bold cursor-pointer"
            onClick={() => navigate("/login")}
          >
            Sign In
          </a>
        </p>
      </div>
    </div>
  );
};

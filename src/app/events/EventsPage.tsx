import EventCard from "@/components/EventCard";
import { events } from "@/mocks/events";

const EventsPage = () => {

  return (
    <div className="px-6 pb-3">
      <div className="px-6 lg:px-0 grid gap-8 place-items-center lg:place-items-start w-full h-full grid-cols-1 md:grid-cols-2 lg:grid-cols-3 2xl:grid-cols-4">
        {events.map((event) => (
          <EventCard key={event.id} challengeEvent={event} />
        ))}
      </div>
    </div>
  );
};

export default EventsPage;

import PageHeader from "@/components/PageHeader";
import { useAuth } from "@/context/authContext";
import { useEffect } from "react";
import { Outlet, useNavigate } from "react-router-dom";

const EventsLayout = () => {
  const navigate = useNavigate();
  const { currentUser } = useAuth();
  useEffect(() => {
    if (!currentUser) {
      navigate("/login");
    }
  }, [currentUser, navigate]);

  return (
    <div>
      <PageHeader pageTitle={"Challenge Events"} piece={0} />
      <Outlet />
    </div>
  );
};

export default EventsLayout;

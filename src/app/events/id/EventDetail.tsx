import BackButton from "@/components/BackButton";
import CustomBadge from "@/components/CustomBadge";
import { Button } from "@/components/ui/button";
import { events } from "@/mocks/events";
import Markdown from "react-markdown";
import { useNavigate, useParams } from "react-router-dom";

const getEvent = (eventId: string) => {
  const id = parseInt(eventId);
  return events[id - 1];
};

const EventDetail = () => {
  const { eventId } = useParams();
  const navigate = useNavigate();

  if (!eventId) return null;

  const event = getEvent(eventId);

  return (
    <div className="prose prose-slate dark:prose-invert pt-8 pb-8 ml-auto max-w-none w-[80%] overflow-auto flex gap-4">
      <BackButton className="absolute left-8 top-24" />
      <div className="max-w-2xl w-full flex-1">
        <h1>{event.title}</h1>
        <Markdown skipHtml={true}>{event.description}</Markdown>
      </div>
      <div className="flex flex-col items-baseline px-4 border-l">
        <h2 className="m-0 mb-2">Prizes :</h2>
        <ul>
          <li>
            <b>First: </b>
            <span className="text-orange-500 text-lg italic">
              {event.prizes.first}
              <span className="italic text-sm text-orange-700">
                &nbsp;pieces
              </span>
            </span>
          </li>
          <li>
            <b>Second: </b>
            <span className="text-orange-500 text-lg italic">
              {event.prizes.second}
              <span className="italic text-sm text-orange-700">
                &nbsp;pieces
              </span>
            </span>
          </li>
          <li>
            <b>Third: </b>
            <span className="text-orange-500 text-lg italic">
              {event.prizes.third}
              <span className="italic text-sm text-orange-700">
                &nbsp;pieces
              </span>
            </span>
          </li>
        </ul>

        <span>
          <b>Badge Requirement : </b>
          <CustomBadge badge={event.requirementBadge} />
        </span>
        <Button
          onClick={() => {
            navigate("deploy");
          }}
          size="lg"
          className="bg-orange-500 hover:bg-orange-400 mt-8 w-60 text-white"
        >
          Get Started
        </Button>
      </div>
    </div>
  );
};

export default EventDetail;

import CardBuy from "@/components/CardBuy";

const BuyPiecePage = () => {
  return (
    <div className="flex flex-col items-center justify-center h-full gap-12">
      <h1 className="text-3xl">Make your choice</h1>
      <div className="flex gap-4">
        <CardBuy nbPieces={100} cost={19900} />
        <CardBuy nbPieces={250} cost={49900} />
        <CardBuy nbPieces={500} cost={99900} />
      </div>
    </div>
  );
};

export default BuyPiecePage;

import { buttonVariants } from "@/components/ui/button";
import { useAuth } from "@/context/authContext";
import { cn } from "@/lib/utils";
import { ArrowRightIcon } from "@radix-ui/react-icons";
import { useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";

const HomePage = () => {
  const navigate = useNavigate();
    const { currentUser } = useAuth()
    useEffect(() => {
        if (currentUser) {
            navigate("/");
        }
    }, [currentUser, navigate]);
  return (
    <div className="relative pt-8 text-center h-full overflow-hidden">
      <h2 className="text-4xl font-semibold">LEARN & EARN</h2>
      <h3 className="text-xl pt-2 text-gray-400">
        Challenge yourself and get rewards
      </h3>
      <Link
        to="/courses"
        className={cn(
          buttonVariants(),
          "my-12 rounded-full bg-blue-500 hover:bg-blue-400 hover:bg-opacity-90 text-foreground text-white hover:shadow-md hover:shadow-blue-200 dark:shadow-none"
        )}
      >
        Get Started &nbsp; <ArrowRightIcon />
      </Link>

      <div className="relative top-32 left-1/2 -translate-x-1/2 -translate-y-1/2 -z-10 w-52 h-80">
        <img
          className="absolute inset-0 h-full w-full object-scale-down"
          src="/dev.png"
          alt=" "
        />
      </div>
    </div>
  );
};

export default HomePage;

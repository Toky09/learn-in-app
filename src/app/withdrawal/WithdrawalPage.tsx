import CardWithdrawal from "@/components/CardWithdrawak";

const WithdrawalPage = () => {
  return (
    <div className="flex flex-col items-center justify-center h-full gap-12">
      <h1 className="text-3xl">Get your Money</h1>
      <div className="flex gap-4">
        <CardWithdrawal nbPieces={1000} cost={69900} />
        <CardWithdrawal nbPieces={2500} cost={149900} />
        <CardWithdrawal nbPieces={4750} cost={339900} />
      </div>
    </div>
  );
};

export default WithdrawalPage;

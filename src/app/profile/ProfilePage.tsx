import CustomBadge from "@/components/CustomBadge";
import { useAuth } from "@/context/authContext";
import { Avatar, AvatarImage, AvatarFallback } from "@radix-ui/react-avatar";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

// type BadgeRequirementsProps = {
//   badge: Badge;
//   requirementPoints: number;
// };

// const badgeRequirements: BadgeRequirementsProps[] = [
//   { badge: "diamond", requirementPoints: 5000 },
//   { badge: "gold", requirementPoints: 4500 },
//   { badge: "silver", requirementPoints: 3750 },
//   { badge: "bronze", requirementPoints: 1250 },
//   { badge: "beginner", requirementPoints: 0 },
// ];

// const getCurrentBadge = (points: number) => {
//   let currentBadge: Badge | undefined;
//   for (let i = 0; i < badgeRequirements.length; i++) {
//     const badgeRequirement = badgeRequirements[i];
//     if (points >= badgeRequirement.requirementPoints) {
//       currentBadge = badgeRequirement.badge;
//       break;
//     }
//   }
//   return currentBadge;
// };

// const getNextBadge = (badge: Badge) => {
//   return
// }

const ProfilePage = () => {
  const navigate = useNavigate();
  const { currentUser } = useAuth();

  useEffect(() => {
    if (!currentUser) {
      navigate("/login");
    }
  }, [currentUser, navigate]);

  if (!currentUser) return null;

  // const currentBadge = getCurrentBadge(currentUser.points);

  return (
    <div className="flex items-center justify-center pt-8">
      <div className="flex flex-col">
        <div className="flex gap-4 pb-4 border-b">
          <Avatar>
            <AvatarImage
              src={currentUser.photoURL! ?? currentUser.image}
              alt={currentUser.displayName ?? currentUser.username}
            />
            <AvatarFallback className="uppercase">
              {currentUser.displayName
                ? currentUser.displayName.slice(0, 2)
                : currentUser.username
                ? currentUser.username.slice(0, 2)
                : "U"}
            </AvatarFallback>
          </Avatar>
          <div className="prose dark:prose-invert">
            <h2>{currentUser.displayName ?? currentUser.username}</h2>
            <div className="flex gap-6">
              <span className="w-fit">Points : {currentUser.points ?? 0}</span>
              <span className="w-fit">Pieces : {currentUser.pieces ?? 0}</span>
              <span className="w-fit">
                Badge : <CustomBadge badge={currentUser.badge ?? "beginner"} />
              </span>
            </div>
          </div>
        </div>

        <div className="pt-4 prose dark:prose-invert">
          <h4>
            Number of courses purchased :{" "}
            <i>{(currentUser.courses ?? []).length}</i>
          </h4>
          <h4>
            Number of challenge participated :{" "}
            <i>{(currentUser.participatedEvents ?? []).length}</i>
          </h4>
        </div>
      </div>
    </div>
  );
};

export default ProfilePage;

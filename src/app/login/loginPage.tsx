import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import * as z from "zod";

import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { toast } from "@/components/ui/use-toast";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { useAuth } from "@/context/authContext";

const FormSchema = z.object({
  username: z.string().min(2, {
    message: "Username must be at least 2 characters.",
  }),
  password: z.string().min(2, {
    message: "Password must be at least 2 characters.",
  }),
});

export const LoginPage = () => {
  const navigate = useNavigate();
  const { currentUser, login, loginWithGoogle } = useAuth();
  useEffect(() => {
    if (currentUser) {
      navigate("/courses");
    }
  }, [currentUser, navigate]);

  const form = useForm<z.infer<typeof FormSchema>>({
    resolver: zodResolver(FormSchema),
    defaultValues: {
      username: "",
      password: "",
    },
  });

  const onSubmit = async (data: z.infer<typeof FormSchema>) => {
    const logindata: login = {
      username: data.username,
      password: data.password,
    };
    try {
      const user = await login(logindata);
      if (user) {
        navigate("/");
        toast({
          title: `Welcome back ` + user?.name,
        });
      }
    } catch (e) {
      console.log("Erreur lors de la connexion avec Google", e);
    }
  };

  const logWithgoogle = async () => {
    try {
      const user = await loginWithGoogle();
      if (user) {
        navigate("/");
        toast({
          title: `Welcome back ` + user?.name,
        });
      }
    } catch (e) {
      console.log("Erreur lors de la connexion avec Google", e);
    }
  };

  return (
    <div className="flex justify-center items-center h-full">
      <div>
        <div className="border rounded-lg border-opacity-0 shadow-md p-4 w-80">
          <div className="text-center mb-4">
            <h1 className="font-bold text-2xl">Welcome back!</h1>
            <h1 className="text-sm">Please enter your details</h1>
          </div>
          <Form {...form}>
            <form
              onSubmit={form.handleSubmit(onSubmit)}
              className="w-full space-y-6"
            >
              <FormField
                control={form.control}
                name="username"
                render={({ field }) => (
                  <>
                    <FormItem>
                      <FormLabel className="font-bold">Email</FormLabel>
                      <FormControl>
                        <Input placeholder="Username or Email" {...field} />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  </>
                )}
              />

              <FormField
                control={form.control}
                name="password"
                render={({ field }) => (
                  <>
                    <FormItem>
                      <FormLabel className="font-bold">Password</FormLabel>
                      <FormControl>
                        <Input
                          type="password"
                          placeholder="Password"
                          {...field}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  </>
                )}
              />

              <Button type="submit" className="w-full font-bold">
                Sign In
              </Button>
            </form>
          </Form>
          <p className="text-center my-2">or</p>
          <Button
            type="button"
            className="w-full font-bold"
            onClick={logWithgoogle}
          >
            Sign In with Google
          </Button>
        </div>
        <p className="text-center mt-2 text-sm">
          You do not have an account ?{" "}
          <a
            className="font-bold cursor-pointer"
            onClick={() => navigate("/regiter")}
          >
            Sign Up
          </a>
        </p>
      </div>
    </div>
  );
};

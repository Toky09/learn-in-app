import PageHeader from "@/components/PageHeader";
import { Outlet } from "react-router-dom";

const CoursesLayout = () => {
  return (
    <div>
      <PageHeader pageTitle={"My courses"} piece={0} />
      <Outlet />
    </div>
  );
};

export default CoursesLayout;

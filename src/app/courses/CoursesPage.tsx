import CardCourse from "@/components/CardCourse";
import { courses } from "@/mocks/courses";
// import { useCours } from "@/context/courseContext";

const CoursesPage = () => {
  // const { allCourses } = useCours();

  // useEffect(() => {
  //   const fetchAllCourse = async () =>{
  //     const courses = await getAllCourses()
  //     console.log(courses![0].courses.title);
  //   }

  //   fetchAllCourse()
  // }, [])
  return (
    <div className="px-6 pb-3">
      <div className="px-6 lg:px-0 grid gap-8 place-items-center lg:place-items-start w-full h-full grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
        {courses.map((course) => (
          <CardCourse
            key={course.id}
            id={course.id}
            title={course.title}
            cost={course.cost}
            imageSrc={course.image}
            difficulty={course.difficulty}
            disabled={course.disabled}
          />
        ))}
      </div>
    </div>
  );
};

export default CoursesPage;

import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import * as z from "zod";

import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { RadioGroup, RadioGroupItem } from "@/components/ui/radio-group";
import { toast } from "@/components/ui/use-toast";
import { courses } from "@/mocks/courses";
import { useParams } from "react-router-dom";

const getCourse = (courseId: string) => {
  const id = parseInt(courseId) - 1;
  return courses[id];
};

const getQuiz = (course: Course, quizId: string) => {
  const id = parseInt(quizId) - 1;
  return course.quizzes[id];
};

export function QuizPage() {
  const { courseId, quizId } = useParams();

  if (!courseId) return null;
  if (!quizId) return null;

  const course = getCourse(courseId);
  const quiz = getQuiz(course, quizId);

  const FormSchema = z.object({
    type: z.enum([quiz.responses[0], quiz.responses[1], quiz.responses[2]], {
      required_error: "You need to select a notification type.",
    }),
  });
  const form = useForm<z.infer<typeof FormSchema>>({
    resolver: zodResolver(FormSchema),
  });

  function onSubmit(data: z.infer<typeof FormSchema>) {
    toast({
      title: "You submitted the following values:",
      description: (
        <pre className="mt-2 w-[340px] rounded-md bg-slate-950 p-4">
          <code className="text-white">{JSON.stringify(data, null, 2)}</code>
        </pre>
      ),
    });
  }

  return (
    <Form {...form}>
      <form
        onSubmit={form.handleSubmit(onSubmit)}
        className="mx-auto w-fit space-y-6 pt-12"
      >
        <FormField
          control={form.control}
          name="type"
          render={({ field }) => (
            <FormItem className="space-y-3">
              <FormLabel className="font-semibold text-3xl">
                {quiz.question}
              </FormLabel>
              <FormControl>
                <RadioGroup
                  onValueChange={field.onChange}
                  defaultValue={field.value}
                  className="flex flex-col space-y-1 ml-6"
                >
                  {quiz.responses.map((response) => (
                    <FormItem
                      className="flex items-center space-x-3 space-y-0"
                      key={response}
                    >
                      <FormControl>
                        <RadioGroupItem value={response} />
                      </FormControl>
                      <FormLabel className="font-normal">{response}</FormLabel>
                    </FormItem>
                  ))}
                </RadioGroup>
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <Button type="submit">Submit</Button>
      </form>
    </Form>
  );
}

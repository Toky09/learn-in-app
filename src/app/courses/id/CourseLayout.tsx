import BackButton from "@/components/BackButton";
import ModuleButton from "@/components/ModuleButton";
import { useAuth } from "@/context/authContext";
import { courses } from "@/mocks/courses";
import { useEffect } from "react";
import { Outlet, useNavigate, useParams } from "react-router-dom";

const getModules = (courseId: string) => {
  const id = parseInt(courseId);
  return courses[id - 1].modules;
};

const CourseLayout = () => {
  const { courseId, moduleId } = useParams();

  const navigate = useNavigate();
  const { currentUser } = useAuth();
  useEffect(() => {
    if (!currentUser) {
      navigate("/login");
    }
  }, [currentUser, navigate]);

  if (!courseId) return null;

  const modules = getModules(courseId);

  return (
    <div className="flex pt-8 px-6 pb-3 gap-6 h-full">
      <BackButton />

      <div className="flex flex-col gap-2 w-[25%] overflow-auto h-full pr-2">
        {modules.map((module) => (
          <ModuleButton
            key={module.id}
            title={module.title}
            disabled={module.disabled}
            id={module.id}
            courseId={courseId}
            activate={module.id === moduleId}
          />
        ))}
      </div>
      <Outlet />
    </div>
  );
};

export default CourseLayout;

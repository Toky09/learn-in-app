import { useAuth } from "@/context/authContext";
import { courses } from "@/mocks/courses";
import { useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Markdown from "react-markdown";
import { Button } from "@/components/ui/button";
import BackButton from "@/components/BackButton";

const getCourse = (courseId: string) => {
  const id = parseInt(courseId);
  return courses[id - 1];
};

const CoursePage = () => {
  const { courseId } = useParams();
  const navigate = useNavigate();

  const { currentUser } = useAuth();
  useEffect(() => {
    if (!currentUser) {
      navigate("/login");
    }
  }, [currentUser, navigate]);

  if (!courseId) return null;

  const course = getCourse(courseId);

  if (course.disabled) throw new Error("Not authorized");

  return (
    <div className="prose prose-slate dark:prose-invert pt-8 pb-8 ml-auto max-w-none w-[80%] overflow-auto flex gap-4">
      <BackButton className="absolute left-8 top-24" />
      <div className="max-w-2xl w-full flex-1">
        <h1>{course.title}</h1>
        <Markdown>{course.description}</Markdown>
      </div>
      <div className="flex flex-col items-baseline px-4">
        <span>
          {" "}
          <b>Cost : </b>
          <span className="font-semibold text-md">{course.cost}</span>
          <span className="italic text-sm">
            &nbsp;piece{course.cost > 1 ?? "s"}
          </span>
        </span>
        <Button
          onClick={() => {
            course.modules[0].disabled = false;
            navigate(`modules/${course.modules[0].id}`);
          }}
          size="lg"
          className="bg-blue-500 hover:bg-blue-400 mt-8 w-60 text-white"
        >
          Busy & Start Learning
        </Button>
      </div>
    </div>
  );
};

export default CoursePage;

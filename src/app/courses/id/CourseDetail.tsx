import { useParams } from "react-router-dom";
import { useAuth } from "@/context/authContext";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Markdown from "react-markdown";
import { courses } from "@/mocks/courses";
import ReactPlayer from "react-player";

const getCourse = (courseId: string) => {
  const id = parseInt(courseId) - 1;
  return courses[id];
};

const getModule = (course: Course, moduleId: string) => {
  const id = parseInt(moduleId) - 1;
  return course.modules[id];
};

const getNextModuleId = (course: Course, currentModuleId: string) => {
  const moduleLength = course.modules.length;
  const id = parseInt(currentModuleId) - 1;
  if (id < moduleLength) {
    return course.modules[id + 1];
  }
};

const CourseDetail = () => {
  const { courseId, moduleId } = useParams();
  const navigate = useNavigate();

  const { currentUser } = useAuth();
  useEffect(() => {
    if (!currentUser) {
      navigate("/login");
    }
  }, [currentUser, navigate]);

  if (!moduleId) return null;
  if (!courseId) return null;

  const course = getCourse(courseId);
  const module = getModule(course, moduleId);
  if (module.disabled) throw new Error("Not authorized");

  const nextModule = getNextModuleId(course, moduleId);

  return (
    <div className="prose prose-slate dark:prose-invert w-[64%] max-w-none overflow-auto h-[100vh-26rem] pr-8">
      <h1>{module.title}</h1>
      <div className="max-w-none w-full flex flex-col gap-4 pb-8">
        <Markdown>{module.description}</Markdown>
        <div className="w-full relative aspect-video bg-foreground/60">
          <ReactPlayer
            className="absolute top-0 left-0"
            controls
            url={module.link}
            width="100%"
            height="100%"
            onEnded={() => {
              if (nextModule) {
                nextModule.disabled = false;
                navigate(`/courses/${courseId}/modules/${nextModule.id}`);
              } else {
                const coursesLength = courses.length;
                const int__courseId = parseInt(course.id) - 1;
                if (int__courseId < coursesLength) {
                  courses[int__courseId + 1].disabled = false;
                }
                navigate(`/courses/${course.id}/quizzes/1`);
              }
            }}
          />
        </div>
      </div>
    </div>
  );
};

export default CourseDetail;

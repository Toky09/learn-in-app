# LEarn-In

Learn & Earn: `LEarn-In` is a platform for tech enthusiasts to participate in coding competitions, collaborate with peers, learning new things, and earn rewards for their coding skills.

Priview link: [LEarn-In](https://learn-in-app.vercel.app/)

## Credits

Image by [freepik](https://www.freepik.com):

- [3d potrait people](https://www.freepik.com/free-ai-image/3d-portrait-people_66108319.htm#query=3d%20developer%20png&position=19&from_view=keyword&track=ais)

Image by [Pixels](https://images.pixels.com)
